#ifndef TRAKTORCONTROLLER_BUTTONBANK_H
#define TRAKTORCONTROLLER_BUTTONBANK_H


#include <cstdint>
#include <MCP23S17.h>
#include <array>
#include <vector>
#include "InputBankInterface.h"

class ButtonBank : public NotesBankInterface {
public:
    ButtonBank(uint8_t bank_1_cs, uint8_t bank_2_cs, uint8_t midiChannel);

    void begin();

    void update();

    void setMidiChannel(uint8_t channel) { midiChannel_ = channel; }

    const std::vector<MidiNote> &getChangedNotes() const override {
        return changedNotes_;
    }

    void clearChangedNotes() override {}

    static constexpr uint8_t N_BUTTONS_IN_BANK = 20;

private:
    static const std::array<std::pair<const uint8_t, const uint8_t>, N_BUTTONS_IN_BANK> BUTTON_TO_BANK; // bank number, index in it
    MCP23S17 bank1_;
    MCP23S17 bank2_;
    uint16_t bank1Value_{};
    uint16_t bank2Value_{};
    uint8_t midiChannel_{1};
    std::vector<MidiNote> changedNotes_;

};


#endif //TRAKTORCONTROLLER_BUTTONBANK_H
