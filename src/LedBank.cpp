#include "LedBank.h"

#define NUM_TLC5974 2

#define DATA_PIN   10
#define CLOCK_PIN   14
#define LATCH_PIN   15
#define OE_PIN  -1  // set to -1 to not use the enable pin (its optional)

const std::array<uint8_t, LedBank::N_LEDS_IN_BANK> LedBank::LEDS = {
        5, 6, 11, 17, 18, 4, 7, 10, 16, 19, 3, 8, 12, 15, 20, 2, 9, 13, 14, 21};

LedBank::LedBank() : tlc(NUM_TLC5974, CLOCK_PIN, DATA_PIN, LATCH_PIN) {}

void LedBank::clear() {
    for (size_t i = 0; i < N_BANKS * N_INDEXABLE_LEDS; ++i) {
        tlc.setPWM(i, 0);
    }
    tlc.write();
}

void LedBank::setLed(uint16_t index, uint16_t value) {
    if (index < N_LEDS) {
        const uint16_t bankOffset = index < N_LEDS_IN_BANK ? 0 : N_INDEXABLE_LEDS;
        index = 0 == bankOffset ? index : index - N_LEDS_IN_BANK;
        value = (value * MAX_LED_VALUE) / 127;
        tlc.setPWM(bankOffset + LEDS[index], value);
    }
}
