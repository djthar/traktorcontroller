#include "ButtonBank.h"

#define BANK_1 1
#define BANK_2 2

const std::array<std::pair<const uint8_t, const uint8_t>, ButtonBank::N_BUTTONS_IN_BANK> ButtonBank::BUTTON_TO_BANK{{
    {BANK_1, 7},  // 1 -> 1,7
    {BANK_1, 10},  // 2 -> 1,10
    {BANK_1, 9},  // 3 -> 1,9
    {BANK_2, 7},  // 4 -> 2,7
    {BANK_2, 8},  // 5 -> 2,8
    {BANK_1, 6},  // 6 -> 1,6
    {BANK_1, 11},  // 7 -> 1,11
    {BANK_1, 8},  // 8 -> 1,8
    {BANK_2, 6},  // 9 -> 2,6
    {BANK_2, 9},  // 10 -> 2,9
    {BANK_1, 5},  // 11 -> 1,5
    {BANK_1, 12},  // 12 -> 1,12
    {BANK_2, 2},  // 13 -> 2,2
    {BANK_2, 5},  // 14 -> 2,5
    {BANK_2, 10},  // 15 -> 2,10
    {BANK_1, 4},  // 16 -> 1,4
    {BANK_1, 13},  // 17 -> 1,13
    {BANK_2, 3},  // 18 -> 2,3
    {BANK_2, 4},  // 19 -> 2,4
    {BANK_2, 11}   // 20 -> 2,11
    }};

ButtonBank::ButtonBank(uint8_t bank_1_cs, uint8_t bank_2_cs, uint8_t midiChannel) : bank1_(&SPI, bank_1_cs, 0),
                                                                                    bank2_(&SPI, bank_2_cs, 0),
                                                                                    midiChannel_(midiChannel) {
}

void ButtonBank::begin() {
    bank1_.begin();
    bank2_.begin();
    for (const auto &button : BUTTON_TO_BANK) {
        if (button.first == BANK_1) {
            bank1_.pinMode(button.second, INPUT_PULLUP);
        } else {
            bank2_.pinMode(button.second, INPUT_PULLUP);
        }
    }
}

void ButtonBank::update() {
    const uint16_t bank1NewValue = bank1_.readPort();
    const uint16_t bank2NewValue = bank2_.readPort();
    changedNotes_.clear();
    changedNotes_.reserve(BUTTON_TO_BANK.size());
    for (size_t i = 0; i < BUTTON_TO_BANK.size(); ++i) {
        const auto &button = BUTTON_TO_BANK[i];
        const uint16_t mask = 0x0001 << button.second;
        const uint16_t buttonNewValue = (button.first == BANK_1 ? bank1NewValue : bank2NewValue) & mask;
        const uint16_t buttonValue = (button.first == BANK_1 ? bank1Value_ : bank2Value_) & mask;
        if (buttonNewValue != buttonValue) {
            changedNotes_.emplace_back(midiChannel_, i, buttonNewValue == 0 ? MidiNote::MAX_VELOCITY : 0);
        }
    }
    bank1Value_ = bank1NewValue;
    bank2Value_ = bank2NewValue;
}
