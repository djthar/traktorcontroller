#ifndef TRAKTORCONTROLLER_INPUTBANKINTERFACE_H
#define TRAKTORCONTROLLER_INPUTBANKINTERFACE_H


#include <vector>
#include <cstdint>
#include <wiring.h>
#include "MidiNote.h"

class NotesBankInterface {
public:
    virtual ~NotesBankInterface() = default;

    virtual const std::vector<MidiNote> &getChangedNotes() const = 0;

    virtual void clearChangedNotes() = 0;
};

class CCsBankInterface {
public:
    virtual ~CCsBankInterface() = default;

    virtual const std::vector<MidiNote> &getChangedCCs() const = 0;

    virtual void clearChangedCCs() = 0;
};


#endif //TRAKTORCONTROLLER_INPUTBANKINTERFACE_H
