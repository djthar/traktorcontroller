#include "Display.h"

#define DISPLAY_CS  2
#define DISPLAY_RST 3
#define DISPLAY_DC  4

Display::Display() :
        tft(DISPLAY_CS, DISPLAY_DC, DISPLAY_RST), decks_{
        DeckDisplay(tft, 0, 0),
        DeckDisplay(tft, 1, 0),
        DeckDisplay(tft, 0, 1),
        DeckDisplay(tft, 1, 1)} {
}

void Display::begin() {
    tft.begin();
    tft.setRotation(3);
    tft.fillScreen(ILI9341_BLACK);
    yield();
    drawSplit4();
    tft.setTextSize(2);
    for (auto &deck : decks_) {
        deck.begin();
    }
    printSnap();
    printQuantize();
}

void Display::drawSplit2() {
    tft.drawFastVLine(tft.width() / 2, 0, tft.height(), ILI9341_RED);
}

void Display::drawSplit4() {
    tft.drawFastVLine(tft.width() / 2, 0, tft.height(), ILI9341_RED);
    tft.drawFastHLine(0, tft.height() / 2, tft.width(), ILI9341_RED);
}

void Display::setLed(uint16_t index, uint16_t value) {
    if (index >= GLOBAL_MAX_OFFSET + 4*DeckDisplay::DECK_MAX_OFFSET) { return; }
    if (index < GLOBAL_MAX_OFFSET) {
        switch (index) {
            case SNAP:
                setSnap(value != 0);
                break;
            case QUANT:
                setQuantize(value != 0);
                break;
            default:
                break;
        }
    } else {
        index -= GLOBAL_MAX_OFFSET;
        const size_t deck = index / DeckDisplay::DECK_MAX_OFFSET;
        index = index % DeckDisplay::DECK_MAX_OFFSET;
        if (deck >= decks_.size()) { return; }
        decks_[deck].setLed(index, value);
    }
}

void Display::DeckDisplay::setLed(uint16_t index, uint16_t value) {
    switch (index) {
        case LOOP_SIZE_LED_OFFSET:
            setLoopSize(static_cast<uint8_t>(value));
            break;
        case LOOP_ACTVE_LED_OFFSET:
            setLoopActive(value != 0);
            break;
        case MASTER_LED_OFFSET:
            setMaster(value != 0);
            break;
        case TRACK_END_LED_OFFSET:
            setTrackEnding(value != 0);
            break;
        case TEMPO_RANGE_LED_OFFSET:
            setTempoRange(static_cast<uint8_t>(value));
            break;
        case TEMPO_RANGE_VALUE_OFFSET:
            setTempoValue(static_cast<uint8_t>(value));
        default:
            break;
    }
}

const char *Display::DeckDisplay::getLoopSizeString(uint8_t loopSize) {
    switch (loopSize) {
        case 0:
            return ".32";
        case 1:
            return ".16";
        case 2:
            return ".8 ";
        case 3:
            return ".4 ";
        case 4:
            return ".2 ";
        case 5:
            return "  1";
        case 6:
            return "  2";
        case 7:
            return "  4";
        case 8:
            return "  8";
        case 9:
            return " 16";
        case 10:
            return " 32";
        default:
            return "  E";
    }
}

uint8_t Display::DeckDisplay::getRangeFromMidi(uint8_t range) {
    switch (range) {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
            return (range + 1) * 2;
        case 10:
            return 25;
        case 11:
            return 35;
        case 12:
            return 50;
        case 13:
            return 100;
        default:
            return 0;
    }
}
