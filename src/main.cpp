#include <Arduino.h>
#include <algorithm>
#include "MidiNote.h"
#include "PhysicalControlWrapper.h"
#include <TimerThree.h>
#include <TeensyThreads.h>
#include <functional>
#include <unordered_map>
#include "json.hpp"

using namespace ArduinoJson6172_91;

PhysicalControlWrapper physicalControlWrapper;

void sendNotesData() {
    const auto &notes = physicalControlWrapper.getChangedNotes();
    std::for_each(notes.begin(), notes.end(), [](const MidiNote &note) {
        if (0 != note.velocity) {
            usbMIDI.sendNoteOn(note.note, note.velocity, note.channel);
        } else {
            usbMIDI.sendNoteOff(note.note, note.velocity, note.channel);
        }
    });
}

void sendCCsData() {
    for (const auto &cc : physicalControlWrapper.getChangedCCs()) {
        usbMIDI.sendControlChange(cc.note, cc.velocity, cc.channel);
    }
}

void onNote(byte channel, byte note, byte velocity) {
    const MidiNote midiNote(channel, note, velocity);
    physicalControlWrapper.setLed(midiNote);
}

void onControlChange(byte channel, byte control, byte value) {}

void processSerialInput() {
    while (Serial.available() > 0) {
        ::String json = Serial.readString();
        Serial.clear();
        if (json.length() == 0) { Serial.println("no length"); return; }
        DynamicJsonDocument doc(1024);
        DeserializationError err = deserializeJson(doc, json);
        if (DeserializationError::Ok != err) { Serial.print(err.c_str()); Serial.print(" "); Serial.println(json); return; }
        const uint8_t deck = doc["deck"];
        if (0 == deck || 4 < deck) { Serial.print("no deck "); Serial.println(json); return; }
        const float bpm = doc["bpm"];
        const float tempo = doc["tempo"];
        const char* tname = doc["tname"];
        Timer3.stop();
        if (0.0f != tempo) { physicalControlWrapper.setTrackTempo(deck, tempo); }
        if (nullptr != tname) { physicalControlWrapper.setTrackName(deck, tname); }
        if (0.0f != bpm) { physicalControlWrapper.setTrackBpm(deck, bpm); }
        Timer3.resume();
        Serial.println("OK ");
    }
}

void ms_loop() {
    static size_t i = 0;
    if (0 == ++i % 10) {
        physicalControlWrapper.updateLeds();
        if (0 == i % 500) {
            physicalControlWrapper.updateDisplayRollFields();
        }
    }
    physicalControlWrapper.update();
    sendNotesData();
    sendCCsData();
    physicalControlWrapper.clearSentData();
    while (usbMIDI.read()) {
    }
}

void setup() {
    physicalControlWrapper.begin();
    usbMIDI.setHandleControlChange(onControlChange);
    usbMIDI.setHandleNoteOff(onNote);
    usbMIDI.setHandleNoteOn(onNote);
    Serial.setTimeout(100);
    Timer3.initialize(1000);
    Timer3.attachInterrupt(ms_loop);
}

void loop() {
    processSerialInput();
}
