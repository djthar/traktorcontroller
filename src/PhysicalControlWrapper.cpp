#include <algorithm>
#include "PhysicalControlWrapper.h"

#define MCP23S17_1_1_CS 19
#define MCP23S17_1_2_CS 18
#define MCP23S17_2_1_CS 17
#define MCP23S17_2_2_CS 16

const std::array<std::pair<uint8_t, uint8_t>, 4> PhysicalControlWrapper::DECK_OFFSET = {{
    {DECK_A_OFFSET, 0},
    {DECK_B_OFFSET, LedBank::N_LEDS_IN_BANK},
    {DECK_C_OFFSET, 0},
    {DECK_D_OFFSET, LedBank::N_LEDS_IN_BANK}
}};

PhysicalControlWrapper::PhysicalControlWrapper() : buttonBank1(MCP23S17_1_1_CS, MCP23S17_1_2_CS, 1),
                                                   buttonBank2(MCP23S17_2_1_CS, MCP23S17_2_2_CS, 2),
                                                   mainBoard(MAIN_BOARD_OFFSET) {
    leds_[DECK_A_OFFSET + BANK_CHANGE_DECK_BUTTON_INDEX] = 0;
    leds_[DECK_B_OFFSET + BANK_CHANGE_DECK_BUTTON_INDEX] = 0;
    leds_[DECK_C_OFFSET + BANK_CHANGE_DECK_BUTTON_INDEX] = 127;
    leds_[DECK_D_OFFSET + BANK_CHANGE_DECK_BUTTON_INDEX] = 127;
}

void PhysicalControlWrapper::begin() {
    mainBoard.begin();
    buttonBank1.begin();
    buttonBank2.begin();
    ledBank.begin();
    updateLeds();
    display.begin();
}

void PhysicalControlWrapper::update() {
    mainBoard.update();
    buttonBank1.update();
    buttonBank2.update();
    updateChangedNotes();
}

void PhysicalControlWrapper::setLed(uint16_t index, uint16_t value) {
    if (index < MAIN_BOARD_OFFSET) {
        if (isLedValid(index)) {
            leds_[index] = value;
            const Deck deck = getDeck(index);
            if (Deck::ERROR != deck) {
                ledBank.setLed(
                        (index % LedBank::N_LEDS_IN_BANK) +
                        DECK_OFFSET[static_cast<uint8_t>(deck)].second, value);
            }
        }
    } else {
        display.setLed(index, value);
    }
}

void PhysicalControlWrapper::setLed(const MidiNote& midiNote) {
    if (0 == midiNote.channel || 5 < midiNote.channel) { return; }
    else if (5 == midiNote.channel) {
        display.setLed(midiNote.note, midiNote.velocity);
        return;
    }
    if (not isLedValid(midiNote.note)) { return; }
    const Deck deck = static_cast<Deck>(midiNote.channel - 1);
    const auto& deckOffset = DECK_OFFSET.at(static_cast<uint8_t>(deck));
    leds_[midiNote.note + deckOffset.first] = midiNote.velocity;
    if (isDeckActive(deck)) {
        ledBank.setLed(midiNote.note + deckOffset.second, midiNote.velocity);
    }
}

void PhysicalControlWrapper::updateChangedNotes() {
    changedNotes_.clear();
    changedNotes_.reserve(2*ButtonBank::N_BUTTONS_IN_BANK + MainBoard::N_DIGITAL_PINS);
    const auto& mainBoardNotes = mainBoard.getChangedNotes();
    std::copy(mainBoardNotes.begin(), mainBoardNotes.end(), std::back_inserter(changedNotes_));
    updateChanegdNotesFromButtonBank(buttonBank1);
    updateChanegdNotesFromButtonBank(buttonBank2);
}

void PhysicalControlWrapper::updateChanegdNotesFromButtonBank(ButtonBank &buttonBank) {
    const auto& notes = buttonBank.getChangedNotes();
    std::copy_if(notes.begin(), notes.end(), std::back_inserter(changedNotes_), [this, &buttonBank](const MidiNote& note){
        if (BANK_CHANGE_DECK_BUTTON_INDEX != note.note) { return true; }
        if (note.velocity == 0) { return false; }
        auto deck = static_cast<Deck>(note.channel - 1);
        switch (deck) {
            case Deck::DECK_A:
            case Deck::DECK_C:
                deckC_ = Deck::DECK_A == deck;
                deck = deckC_ ? Deck::DECK_C : Deck::DECK_A;
                buttonBank.setMidiChannel(deckC_ ? 3 : 1);
                break;
            case Deck::DECK_B:
            case Deck::DECK_D:
                deckD_ = Deck::DECK_B == deck;
                deck = deckD_ ? Deck::DECK_D : Deck::DECK_B;
                buttonBank.setMidiChannel(deckD_ ? 4 : 2);
                break;
            case Deck::ERROR:
                return false;
        }
        setDeckLeds(deck);
        return false;
    });
}

void PhysicalControlWrapper::updateLeds() {
    ledBank.update();
}

void PhysicalControlWrapper::updateDisplayRollFields() {
    display.update();
}