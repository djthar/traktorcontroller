#ifndef TRAKTORCONTROLLER_PHYSICALCONTROLWRAPPER_H
#define TRAKTORCONTROLLER_PHYSICALCONTROLWRAPPER_H

#include "InputBankInterface.h"
#include "MainBoard.h"
#include "ButtonBank.h"
#include "LedBank.h"
#include "Display.h"
#include "MidiNote.h"

class PhysicalControlWrapper : public NotesBankInterface, public CCsBankInterface {
public:
    PhysicalControlWrapper();

    const std::vector<MidiNote> &getChangedNotes() const override { return changedNotes_; }

    void clearChangedNotes() override {
        mainBoard.clearChangedNotes();
        buttonBank1.clearChangedNotes();
        buttonBank2.clearChangedNotes();
        changedNotes_.clear();
    }

    const std::vector<MidiNote> &getChangedCCs() const override { return mainBoard.getChangedCCs(); }

    void clearChangedCCs() override {
        mainBoard.clearChangedCCs();
    }

    void begin();

    void update();

    void setLed(uint16_t index, uint16_t value);

    void setLed(const MidiNote& midiNote);

    bool isDeckAActive() const { return !deckC_; }

    bool isDeckBActive() const { return !deckD_; }

    void clearSentData() {
        clearChangedNotes();
        clearChangedCCs();
    }

    void updateLeds();

    void updateDisplayRollFields();

    void setTrackName(uint8_t deck, const std::string &trackName) {
        if (isDeckNumberValid(deck)) {
            display.setTrackName(deck - 1, trackName);
        }
    }

    void setTrackBpm(uint8_t deck, float bpm) {
        if (isDeckNumberValid(deck)) {
            display.setTrackBpm(deck - 1, bpm);
        }
    }

    void setTrackTempo(uint8_t deck, float tempo) {
        if (isDeckNumberValid(deck)) {
            display.setTrackTempo(deck - 1, tempo);
        }
    }

protected:
    enum class Deck {
        DECK_A = 0,
        DECK_B = 1,
        DECK_C = 2,
        DECK_D = 3,
        ERROR = 4
    };

    static bool isDeckNumberValid(uint8_t deck) {
        return 0 < deck && 4>= deck;
    }

    void updateChangedNotes();

    void updateChanegdNotesFromButtonBank(ButtonBank& buttonBank);

    static bool isDeckAIndex(uint16_t index) { return index < DECK_B_OFFSET; }

    static bool isDeckBIndex(uint16_t index) { return index >= DECK_B_OFFSET && index < DECK_C_OFFSET; }

    static bool isDeckCIndex(uint16_t index) { return index >= DECK_C_OFFSET && index < DECK_D_OFFSET; }

    static bool isDeckDIndex(uint16_t index) { return index >= DECK_D_OFFSET && index < MAIN_BOARD_OFFSET; }

    bool isDeckActive(const Deck &deck) const {
        switch (deck) {
            case Deck::DECK_A:
                return !deckC_;
            case Deck::DECK_B:
                return !deckD_;
            case Deck::DECK_C:
                return deckC_;
            case Deck::DECK_D:
                return deckD_;
            case Deck::ERROR:
                return false;
        }
        return false;
    }

    Deck getDeck(uint16_t ledIndex) const {
        if (isDeckAIndex(ledIndex) && !deckC_) {
            return Deck::DECK_A;
        } else if (isDeckBIndex(ledIndex) && !deckD_) {
            return Deck::DECK_B;
        } else if (isDeckCIndex(ledIndex) && deckC_) {
            return Deck::DECK_C;
        } else if (isDeckDIndex(ledIndex) && deckD_) {
            return Deck::DECK_D;
        }
        return Deck::ERROR;
    }

    static bool isLedValid(uint16_t index) {
        return BANK_CHANGE_DECK_BUTTON_INDEX != index % LedBank::N_LEDS or index >= LedBank::N_LEDS;
    }

    void setDeckLeds(Deck deck) {
        if (Deck::ERROR == deck) { return; }
        const std::pair<uint8_t, uint8_t> offsets = DECK_OFFSET.at(static_cast<uint8_t>(deck));
        for (size_t i = 0; i < LedBank::N_LEDS_IN_BANK; ++i) {
            ledBank.setLed(offsets.second + i, leds_[offsets.first + i]);
        }
    }

private:
    ButtonBank buttonBank1;
    ButtonBank buttonBank2;
    LedBank ledBank;
    MainBoard mainBoard;
    Display display;
    std::vector<MidiNote> changedNotes_;
    bool deckC_{false};
    bool deckD_{false};
    static constexpr uint8_t N_LAYERS = 2;
    std::array<uint16_t, N_LAYERS * LedBank::N_LEDS> leds_{};
    static constexpr uint8_t DECK_A_OFFSET = 0 * ButtonBank::N_BUTTONS_IN_BANK;
    static constexpr uint8_t DECK_B_OFFSET = 1 * ButtonBank::N_BUTTONS_IN_BANK;
    static constexpr uint8_t DECK_C_OFFSET = 2 * ButtonBank::N_BUTTONS_IN_BANK;
    static constexpr uint8_t DECK_D_OFFSET = 3 * ButtonBank::N_BUTTONS_IN_BANK;
    static constexpr uint8_t MAIN_BOARD_OFFSET = 4 * ButtonBank::N_BUTTONS_IN_BANK;
    static constexpr uint8_t BANK_CHANGE_DECK_BUTTON_INDEX = 10;
    static const std::array<std::pair<uint8_t, uint8_t>, 4> DECK_OFFSET;

};


#endif //TRAKTORCONTROLLER_PHYSICALCONTROLWRAPPER_H
