#ifndef TRAKTORCONTROLLER_DISPLAY_H
#define TRAKTORCONTROLLER_DISPLAY_H

#include <Adafruit_ILI9341.h>
#include <array>
#include <string>
#include <algorithm>

class Display {
public:
    Display();

    void begin();

    void drawSplit2();

    void drawSplit4();

    void setLed(uint16_t index, uint16_t value);

    void setTrackName(uint8_t deck, const std::string &trackName) {
        if (deck < decks_.size()) {
            decks_[deck].setTrackName(trackName);
        }
    }

    void setTrackBpm(uint8_t deck, float bpm) {
        if (deck < decks_.size()) {
            decks_[deck].setBpm(bpm);
        }
    }

    void setTrackTempo(uint8_t deck, float bpm) {
        if (deck < decks_.size()) {
            decks_[deck].setTempo(bpm);
        }
    }

    void update() {
        for (auto &deck : decks_) {
            deck.update();
        }
    }

private:
    enum GLOBAL_LED_OFFSET {
        SNAP = 0, // 0 -> C-1
        GLOBAL_2,
        QUANT,    // 2 -> D-1
        GLOBAL_4,
        GLOBAL_5,
        GLOBAL_MAX_OFFSET
    };

    class DeckDisplay {
    public:
        explicit DeckDisplay(Adafruit_ILI9341 &tft, uint8_t x, uint8_t y) : tft_(tft), x_(x), y_(y) {}

        void setMaster(bool value) {
            if (value != isMaster_) {
                isMaster_ = value;
                printMaster();
            }
        }

        void setLoopActive(bool value) {
            if (value != isLoopActive_) {
                isLoopActive_ = value;
                printLoop();
            }
        }

        void setTrackEnding(bool value) {
            if (value != isTrackEnding_) {
                isTrackEnding_ = value;
                printEnd();
            }
        }

        void setBpm(float value) {
            if (value != bpm_) {
                bpm_ = value;
                printBpm();
            }
        }

        void setTempoRange(uint8_t value) {
            value = getRangeFromMidi(value);
            if (0 != value && value != tempoRange_) {
                tempoRange_ = value;
                printTempoRange();
            }
        }

        void setTempo(float value) {
            const float tempoRange = static_cast<float>(tempoRange_) / 100.0f;
            if (0 == tempoRange) { return; }
            const float newTempoValue = (value-1) / tempoRange;
            if (newTempoValue != tempoValue_) {
                tempoValue_ = newTempoValue;
                printBpm();
            }
        }

        void setTempoValue(uint8_t value) {
            float tempoValue = (63 == value || 64 == value) ? 0 : 2 * static_cast<float>(value) / 127 - 1;
            if (tempoValue != tempoValue_) {
                tempoValue_ = tempoValue;
                printBpm();
            }
        }

        void setLoopSize(uint8_t value) {
            if (value != loopSize_) {
                loopSize_ = value;
                printLoop();
            }
        }

        void setLed(uint16_t index, uint16_t value);

        void setTrackName(const std::string &trackName) {
            if (trackName != trackName_) {
                trackName_ = trackName;
                printedTrackName_ = trackName.size() > MAX_PRINT_CHARS ? trackName + "   " : trackName;
                printTrackName();
            }
        }

        float getBpm() const {
            const float tempoRange = static_cast<float>(tempoRange_) / 100.0f;
            return bpm_ * (1 + tempoRange * tempoValue_);
        }

        void begin() {
            printMaster();
            printTempoRange();
            printBpm();
            printLoop();
            printEnd();
        }

        void update() {
            if (trackName_.size() > MAX_PRINT_CHARS) {
                leftRotatePrintedTrackName();
                printTrackName();
            }
        }

        template<typename T>
        void print(uint8_t row, uint8_t col, T value, uint16_t background = ILI9341_BLACK,
                   uint16_t textColor = ILI9341_WHITE) {
            static const uint16_t deckWidth = tft_.width() / 2;
            static const uint16_t deckHeight = tft_.height() / 2;
            static const uint16_t rowHeight = deckHeight / 6;
            static const uint16_t colWidth = deckWidth / 6;
            const uint16_t x = (x_ * deckWidth) + (col * colWidth) + 2;
            const uint16_t y = (y_ * deckHeight) + (row * (rowHeight + 10)) + 2;
            tft_.setTextColor(textColor, background);
            tft_.setCursor(x, y);
            tft_.print(value);
        }

        enum DECK_LED_OFFSET {
            LOOP_SIZE_LED_OFFSET = 0,   // 5 14 23 32 ->  F-1  D0  B0 G#1
            LOOP_ACTVE_LED_OFFSET,      // 6 15 24 33 -> F#-1 D#0  C1  A1
            MASTER_LED_OFFSET,          // 7 16 25 34 ->  G-1  E0 C#1 A#1
            TRACK_END_LED_OFFSET,       // 8 17 26 35 -> G#-1  F0  D1  B1
            TEMPO_RANGE_LED_OFFSET,     // 9 18 27 36 ->  A-1 F#0 D#1  C2
            TEMPO_RANGE_VALUE_OFFSET,   //10 19 28 37 -> A#-1  G0  E1 C#2
            RESERVED_1,                 //11 20 29 38 ->  B-1 G#0  F1  D2
            RESERVED_2,                 //12 21 30 39 ->   C0  A0 F#1 D#2
            RESERVED_3,                 //13 22 31 40 ->  C#0 A#0  G1  E2
            DECK_MAX_OFFSET
        };

    private:
        void leftRotatePrintedTrackName() {
            std::reverse(printedTrackName_.begin(), printedTrackName_.begin()+1);
            std::reverse(printedTrackName_.begin()+1, printedTrackName_.end());
            std::reverse(printedTrackName_.begin(), printedTrackName_.end());
        }

        static const char *getLoopSizeString(uint8_t loopSize);

        static uint8_t getRangeFromMidi(uint8_t range);

        void printMaster() {
            print(0, 0, "MAST", isMaster_ ? ILI9341_GREEN : ILI9341_BLACK, isMaster_ ? ILI9341_BLACK : ILI9341_WHITE);
        }

        void printTempoRange() {
            char tempo[6]{};
            snprintf(tempo, sizeof(tempo), " %3d%%", tempoRange_);
            print(0, 2, tempo, ILI9341_BLACK);
        }

        void printTrackName() {
            char trackName[MAX_PRINT_CHARS + 1];
            snprintf(trackName, MAX_PRINT_CHARS+1, "%s", printedTrackName_.c_str());
            print(1, 0, trackName);
        }

        void printBpm() {
            char bpm[11]{};
            snprintf(bpm, sizeof(bpm), "%3.2f bpm", getBpm());
            print(2, 0, bpm, ILI9341_BLACK);
        }

        void printLoop() {
            print(3, 0, getLoopSizeString(loopSize_), isLoopActive_ ? ILI9341_GREEN : ILI9341_BLACK,
                  isLoopActive_ ? ILI9341_BLACK : ILI9341_WHITE);
        }

        void printEnd() {
            print(3, 2, "END", isTrackEnding_ ? ILI9341_RED : ILI9341_BLACK);
        }

        Adafruit_ILI9341 &tft_;
        bool isMaster_{false};
        bool isLoopActive_{false};
        bool isTrackEnding_{false};
        std::string trackName_;
        std::string printedTrackName_;
        float bpm_{120.0f};
        float tempoValue_{0};
        uint8_t tempoRange_{8};
        uint8_t loopSize_{7};
        const uint8_t x_;
        const uint8_t y_;
        static const uint8_t MAX_PRINT_CHARS = 13;
    };

    void setSnap(bool value) {
        if (value != isSnap_) {
            isSnap_ = value;
            printSnap();
        }
    }

    void setQuantize(bool value) {
        if (value != isQuantize_) {
            isQuantize_ = value;
            printQuantize();
        }
    }

    void printSnap() {
        decks_[1].print(3, 5, "S", isSnap_ ? ILI9341_GREEN : ILI9341_BLACK, isSnap_ ? ILI9341_BLACK : ILI9341_WHITE);
    }

    void printQuantize() {
        decks_[3].print(0, 5, "Q", isQuantize_ ? ILI9341_GREEN : ILI9341_BLACK,
                        isQuantize_ ? ILI9341_BLACK : ILI9341_WHITE);
    }

    Adafruit_ILI9341 tft;
    std::array<DeckDisplay, 4> decks_;
    bool isSnap_{false};
    bool isQuantize_{false};
};


#endif //TRAKTORCONTROLLER_DISPLAY_H
