#ifndef TRAKTORCONTROLLER_LEDBANK_H
#define TRAKTORCONTROLLER_LEDBANK_H


#include <Adafruit_TLC5947.h>
#include <array>

class LedBank {
public:
    LedBank();

    void begin() { tlc.begin(); }

    void clear();

    void update() { tlc.write(); }

    void setLed(uint16_t index, uint16_t value);

    static constexpr uint8_t N_BANKS = 2;
    static constexpr uint8_t N_LEDS_IN_BANK = 20;
    static constexpr uint8_t N_LEDS = N_BANKS * N_LEDS_IN_BANK;
    static constexpr uint16_t MAX_LED_VALUE = (2*4095)/3;

private:
    static const std::array<uint8_t, N_LEDS_IN_BANK> LEDS;
    static const uint8_t N_INDEXABLE_LEDS = 24;
    Adafruit_TLC5947 tlc;

};


#endif //TRAKTORCONTROLLER_LEDBANK_H
