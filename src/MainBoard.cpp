#include <pins_arduino.h>
#include "MainBoard.h"

#define ENCODER_CHANNEL 1
#define ENCODER_PIN_1 0
#define ENCODER_PIN_2 1

const std::array<int, 4> MainBoard::ANALOG_PINS = {{A6, A7, A8, A9}};
const std::array<int, 6> MainBoard::DIGITAL_PINS = {{8, 7, 6, 5, 9, 24}};
const int KNOB_TO_CCID[4] = {21, 22, 23, 24};
const int ENCODER_CCID = 25;
const uint8_t MIDI_CHANNEL = 5;

ADC* adc = nullptr;
MainBoard::AnalogAdcPairController* analogStructAdc0 = nullptr;
MainBoard::AnalogAdcPairController* analogStructAdc1 = nullptr;

void adc0_isr(void) {
    if (!adc or !analogStructAdc0) {return;}
    analogStructAdc0->setValue(adc->adc0->readSingle());
    analogStructAdc0->change();
    adc->adc0->startSingleRead(analogStructAdc0->getPin());
}

void adc1_isr(void) {
    if (!adc or !analogStructAdc1) {return;}
    analogStructAdc1->setValue(adc->adc1->readSingle());
    analogStructAdc1->change();
    adc->adc1->startSingleRead(analogStructAdc1->getPin());
}

MainBoard::MainBoard(uint8_t digitalButtonsOffset) : encoder(ENCODER_CHANNEL, ENCODER_PIN_1, ENCODER_PIN_2), analogStruct_{{A6, A7}, {A8, A9}}, digitalButtonsOffset_(digitalButtonsOffset) {}

void MainBoard::begin() {
    for (int i : DIGITAL_PINS) {
        pinMode(i, INPUT_PULLUP);
    }
    for (int i : ANALOG_PINS) {
        pinMode(i, INPUT);
    }
    adc = &adc_;
    analogStructAdc0 = &analogStruct_[0];
    analogStructAdc1 = &analogStruct_[1];
    adc_.adc0->setAveraging(16);                                    // set number of averages
    adc_.adc0->setResolution(10);                                   // set bits of resolution
    adc_.adc0->setConversionSpeed(ADC_CONVERSION_SPEED::MED_SPEED); // change the conversion speed
    adc_.adc0->setSamplingSpeed(ADC_SAMPLING_SPEED::MED_SPEED);     // change the sampling speed
    adc_.adc0->enableInterrupts(adc0_isr);
    adc_.adc1->setAveraging(16);                                    // set number of averages
    adc_.adc1->setResolution(10);                                   // set bits of resolution
    adc_.adc1->setConversionSpeed(ADC_CONVERSION_SPEED::MED_SPEED); // change the conversion speed
    adc_.adc1->setSamplingSpeed(ADC_SAMPLING_SPEED::MED_SPEED);     // change the sampling speed
    adc_.adc1->enableInterrupts(adc1_isr);
    adc_.adc0->startSingleRead(analogStructAdc0->getPin());
    adc_.adc1->startSingleRead(analogStructAdc1->getPin());
    encoder.setInitConfig();
    encoder.init();
}

void MainBoard::update() {
    changedNotes_.clear();
    changedNotes_.reserve(DIGITAL_PINS.size());
    changedCCs_.clear();
    changedCCs_.reserve(ANALOG_PINS.size() + 1); //1 from encoder
    updateAnalog();
    updateDigital();
    updateEncoder();
}

void MainBoard::updateDigital() {
    for (size_t i = 0; i < DIGITAL_PINS.size(); ++i) {
        digital[i].update();
        if (digital[i].fallingEdge()) {
            changedNotes_.emplace_back(MIDI_CHANNEL, digitalButtonsOffset_ + i, MidiNote::MAX_VELOCITY);
        } else if (digital[i].risingEdge()) {
            changedNotes_.emplace_back(MIDI_CHANNEL, digitalButtonsOffset_ + i, 0);
        }
    }
}

void MainBoard::updateAnalog() {
    for (size_t i = 0; i < ANALOG_PINS.size(); ++i) {
        analog[i].update(analogStruct_[i<2 ? 0 : 1].getPinValue(i%2));
        if (analog[i].hasChanged()) {
            const auto analogValue = static_cast<uint16_t>(analog[i].getValue()<<4);
            if (analogValue != analogValues_[i]) {
                analogValues_[i] = analogValue;
                const auto lowAnalogValue = static_cast<uint8_t>(analogValue & MidiNote::MAX_VELOCITY);
                const auto highAnalogValue = static_cast<uint8_t>((analogValue>>7) & MidiNote::MAX_VELOCITY);
                changedCCs_.emplace_back(MIDI_CHANNEL, KNOB_TO_CCID[i], highAnalogValue);
                changedCCs_.emplace_back(MIDI_CHANNEL, KNOB_TO_CCID[i]+32, lowAnalogValue);
            }
        }
    }
}

void MainBoard::updateEncoder() {
    const int32_t encoderValue = encoder.read();
    if (0 == encoderValue%4 && encoderValue != lastEncoderValue_) {
        changedCCs_.emplace_back(MIDI_CHANNEL, ENCODER_CCID, encoderValue > lastEncoderValue_ ? MidiNote::MAX_VELOCITY : 1);
        lastEncoderValue_ = encoderValue;
    }
}
