#ifndef TRAKTORCONTROLLER_MIDINOTE_H
#define TRAKTORCONTROLLER_MIDINOTE_H

#include <cstdint>

struct MidiNote {
    MidiNote(uint8_t c, uint8_t n, uint8_t v) : channel(c), note(n), velocity(v) {}
    uint8_t channel{1};
    uint8_t note{0};
    uint8_t velocity{};
    static constexpr uint8_t MAX_VELOCITY = 127;
};


#endif //TRAKTORCONTROLLER_MIDINOTE_H
