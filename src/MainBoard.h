#ifndef TRAKTORCONTROLLER_MAINBOARD_H
#define TRAKTORCONTROLLER_MAINBOARD_H


#include <ResponsiveAnalogRead.h>
#include <Bounce.h>
#include <vector>
#include <QuadEncoder.h>
#include <array>
#include "InputBankInterface.h"
#include <ADC.h>

class MainBoard : public NotesBankInterface,
                  public CCsBankInterface {
public:
    explicit MainBoard(uint8_t digitalButtonsOffset=0);

    const std::vector<MidiNote> &getChangedNotes() const override { return changedNotes_; }

    const std::vector<MidiNote> &getChangedCCs() const override { return changedCCs_; }

    void clearChangedNotes() override { changedNotes_.clear(); }

    void clearChangedCCs() override { changedCCs_.clear(); }

    void update();

    void begin();

    class AnalogAdcPairController {
    public:
        AnalogAdcPairController(int pin1, int pin2) : pins{pin1, pin2} {}
        int getPin() const { return pins[actualPosition_]; }
        void setValue(int value) { pinsValue[actualPosition_] = value; }
        void change() { actualPosition_ = 0 == actualPosition_ ? 1 : 0; }
        int getPinValue(size_t index) const { return pinsValue[index]; }
    private:
        int pins[2]{};
        int pinsValue[2]{};
        size_t actualPosition_{};
    };

    static constexpr uint8_t N_ANALOG_PINS = 4;
    static constexpr uint8_t N_DIGITAL_PINS = 6;

protected:
    void updateDigital();

    void updateAnalog();

    void updateEncoder();

private:
    static const std::array<int, N_ANALOG_PINS> ANALOG_PINS;
    static const std::array<int, N_DIGITAL_PINS> DIGITAL_PINS;
    static const uint8_t BOUNCE_TIME = 5;
    std::array<int, ANALOG_PINS.size()> analogValues_{};
    ResponsiveAnalogRead analog[ANALOG_PINS.size()]{
            {ANALOG_PINS[0], true},
            {ANALOG_PINS[1], true},
            {ANALOG_PINS[2], true},
            {ANALOG_PINS[3], true},
    };
    Bounce digital[DIGITAL_PINS.size()] = {
            Bounce(DIGITAL_PINS[0], BOUNCE_TIME),
            Bounce(DIGITAL_PINS[1], BOUNCE_TIME),
            Bounce(DIGITAL_PINS[2], BOUNCE_TIME),
            Bounce(DIGITAL_PINS[3], BOUNCE_TIME),
            Bounce(DIGITAL_PINS[4], BOUNCE_TIME),
            Bounce(DIGITAL_PINS[5], BOUNCE_TIME),
    };

    QuadEncoder encoder;
    int32_t lastEncoderValue_{};
    std::vector<MidiNote> changedNotes_;
    std::vector<MidiNote> changedCCs_;
    ADC adc_;
    AnalogAdcPairController analogStruct_[2];
    const uint8_t digitalButtonsOffset_{};
};


#endif //TRAKTORCONTROLLER_MAINBOARD_H
